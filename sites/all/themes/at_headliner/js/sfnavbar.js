(function ($) {
  Drupal.behaviors.atheadliner = {
    attach: function(context) {
	  if ($('#menu-bar ul').hasClass('sf-navbar')) {
	    $('#menu-bar .block').addClass('dark-arrow');
        $('#menu-bar').after('<div id="sf-navbar-background" class="menu-bottom"></div>');
      }
    }
  };
})(jQuery);