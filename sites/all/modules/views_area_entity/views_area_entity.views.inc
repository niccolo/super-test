<?php

/**
 * @file
 * Integrates with the views module.
 *
 * Implements hook_views_data().
 */
function views_area_entity_views_data() {
  $data['views']['views_area_entity_text'] = array(
    'title' => t('Fieldable area'),
    'help' => t('Displays an custom content item within a given display area.'),
    'area' => array(
      'help' => t('Provides an editable entity configurable outside of the main view administration UI.'),
      'handler' => 'views_area_entity_area_text',
    ),
  );
  return $data;
}

